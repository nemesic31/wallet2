import React from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import { StackNavigator } from 'react-navigation';
import Login from './src/Login';
import Profile from './src/Profile';
import User from './src/User';
import Bill from './src/Bill';
import Scan from './src/Scan';
import Signup from './src/Signup';
import Payment from './src/Payment';
import History from './src/History';
import firebase from 'firebase';

const Application = StackNavigator({
  Home: { screen: Login },
  Profile: { screen: Profile },
  User: { screen: User },
  Scan: { screen: Scan },
  Signup: { screen: Signup },
  Bill: { screen: Bill },
  Payment: { screen: Payment },
  History: { screen: History }
},{
  navigationOptions: {
    header: false,
  }

});

export default class App extends React.Component {

  componentWillMount(){
var config = {
    apiKey: "AIzaSyBOBgxI1SVjQG-WUWgYy_BkALKCvkPA7z4",
    authDomain: "mwallet-1234.firebaseio.com",
    databaseURL: "https://mwallet-1234.firebaseio.com/",
    projectId: "mwallet-1234",
    storageBucket: "mwallet-1234.appspot.com",
    messagingSenderId: "881584574792"
  };
  firebase.initializeApp(config);
  }


  render() {
    return (
      <Application />
      
    );
  }
}

const styles = StyleSheet.create({
 
});
