import React from 'react';
import { StyleSheet, Text, View,
        TextInput,
        KeyboardAvoidingView, 
        TouchableOpacity, 
        AsyncStorage,ImageBackground,Image,
    Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { parse } from 'ipaddr.js';
import firebase from 'firebase';

export default class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            repassword: '',
        }
    }

  render() {
    const { navigate } = this.props.navigation;
    return (
        <ImageBackground 
      source={require('./img/bg.jpg')}
      style={styles.container1}           >
        <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>

      <View style={styles.container}>
      <Image style={styles.logo} source={require('./img/logo.png')} />
          <TextInput
          style={styles.textInput} placeholder='Username'
          onChangeText={(username) => this.setState({username})}
          
          underlineColorAndroid='transparent'
          />

          <TextInput
          style={styles.textInput} placeholder='Password'
          onChangeText={(password) => this.setState({password})}
          secureTextEntry={true}
          underlineColorAndroid='transparent'
          />

          <TextInput
          style={styles.textInput} placeholder='RePassword'
          onChangeText={(Repassword) => this.setState({repassword})}
          secureTextEntry={true}
          underlineColorAndroid='transparent'
          />

        <TouchableOpacity
        style={styles.btn}
        onPress={() => {this.signup()}}>
        <Text style={{color:"#fff"}}>Sign Up</Text>
        </TouchableOpacity>
        <View style={styles.signupTextCont}>
        <Text style={styles.signupText}>Already have an account? </Text>
                <TouchableOpacity onPress={() => navigate('Home')}>
                    <Text style={styles.signupButton}>SignIn </Text>
                </TouchableOpacity>
                </View >
      </View>
      </KeyboardAvoidingView>
      </ImageBackground>
    );
  }


 async signup(){

    await firebase.database().ref("users").on("value",(data) => {
        let user = username
        // alert(user[this.state.username])

        let pass = password
        if (pass & user != 'null'){
            this.props.navigation.navigate('Login');
        }
        else {
            alert("Please Enter Information")
                  }

     } )
// console.log("-----------Login----------")
//      let user = await fetch('https://mwallet-1234.firebaseio.com/users.json')
//         alert(user.tor);
        // JSON parse
    //   .then((response) => response.json())
    //   .then((res) => {

    //     alert(res.message);

    //     if (res.success === true) {
    //         AsyncStorage.setItem('user',res.user);
    //         this.props.navigation.navigate('Profile');
    //     }

    //     else {
    //         alert(res.message);
//         }
//       })
      
    
//     .done();
    
//   }
// }
    }
}

const styles = StyleSheet.create ({
    signupTextCont:{
        // flexGrow: 1,
        // alignItems: 'flex-end',
        justifyContent: 'center',
        marginVertical: 16,
        flexDirection: 'row',
        
    },
    signupText: {
        color: "#3e5c7a",
        fontSize: 16,
    },
    signupButton: {
        color: "#203e5b",
        fontSize: 16,
        fontWeight: '500',
    },
    logo: {
        width: 150,
        height: 150
    },
    container1: {
        flex: 1,
        width: '100%',
        height: '100%',
      },
    wrapper: {
        flex: 1,
        
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40,
    },
    header: {
        fontSize: 24,
        marginBottom: 60,
        color: '#fff',
        fontWeight: 'bold',
    },
    textInput: {
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#fff',
        marginVertical: 10,
        borderRadius: 25,
    },
    btn: {
        alignSelf: 'stretch',
        backgroundColor: '#173970',
        padding: 20,
        alignItems: 'center',
        borderRadius: 25,
    }
});
