import React from 'react';
import { StyleSheet, Text, View,
        TextInput,
        KeyboardAvoidingView, 
        TouchableOpacity, 
        ImageBackground,Button,Image, 
        AsyncStorage,FlatList } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Card } from 'react-native-elements';
import QRCode from 'react-native-qrcode';


export default class History extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: '',
      id:'',
      histories:[]

    }
    

   
    this.loaddata()
    // console.log("3-------------->" + this.state.user.amount)
}

 
  async loaddata(){
    
    
    let userid =  await AsyncStorage.getItem('@User:key');
    let myuserid =  await AsyncStorage.getItem('@UserId:key');
    let user = JSON.parse(userid);
    this.setState({user: JSON.parse(userid)})
    this.setState({id: myuserid})
    console.log(user)

    var res = Object.keys(user.histories)
            // iterate over them and generate the array
            .map(function(k) {
              // generate the array element 
              return user.histories[k];
            });
            console.log("-----------------4" + user.histories)
            console.log(res[0])
    // this.setState({historydate: userid.history[date]})
            this.setState({histories: res});

   
    // console.log("4-------------->" + userid)
          console.log(this.state.histories)
    
  }




  render() {
    const { navigate } = this.props.navigation;
  
    return (
      <ImageBackground 
        source={require('./img/payment.jpg')}
        style={styles.container1}           >
         <View style={styles.container}>

<View style={{marginTop:10}}>
  <TouchableOpacity
        style={styles.btn}
        onPress={() => navigate('User')}>
        <Text style={{color:"#fff"}}>Back</Text>
        </TouchableOpacity>
</View>

<View style={{marginVertical: 20}}>
  <View style={{alignItems: 'center',justifyContent: 'center'}}>
  <Card title='History' >
 {/* <Text style={{fontSize: 18}}>Partner ID: {this.state.user.Iduser}</Text>
 <Text style={{fontSize: 18}}>{this.state.user.amount} bath</Text>
 <Text style={{fontSize: 18}}>Date: {this.state.user.date}</Text>
 <Text style={{fontSize: 18}}>My account: {this.state.user.firstname}</Text>
 <Text style={{fontSize: 18}}>Balance: {this.state.user.balance} ฿</Text> */}
 <FlatList
  data={this.state.histories}
  renderItem={({item}) => <Text >{item.date} : {item.Iduser} {item.amount}</Text>}
  keyExtractor={(item, index) => index.toString()}
/>
 {/* <Text style={{fontSize: 18}}>{this.state.user.histories}</Text> */}
</Card>
</View>
</View>

       




          

        
        
          </View>
       </ImageBackground>
    );
    
  }
  
}



const styles = StyleSheet.create ({
  card: {
    width: '50%',
    height: '50%',
  },
  menuItem: {
    width: '33.333333%',
    height: '50%',
    padding: 20,
  },
  image: {
    width: '100%',
    height: '100%',
    opacity: 0.8,
    borderColor: '#fff',
    borderWidth: 3,
  },
  container1: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    // backgroundColor: '#2896d3',
    paddingLeft: 5,
    paddingRight: 5,
    marginVertical: 40
  },

   text: {
       color: '#fff',
   },
   btn: {
    marginVertical: 10,
    alignSelf: 'stretch',
    backgroundColor: '#173970',
    padding: 20,
    alignItems: 'center',
    borderRadius: 25,
},
logo: {
  width: 150,
  height: 150
},
});
