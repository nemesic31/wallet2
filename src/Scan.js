import React from 'react';
import {
  StyleSheet, Text, View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  ImageBackground, Button, Image,
  AsyncStorage
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { BarCodeScanner, Permissions } from 'expo';
import firebase from 'firebase';

export default class Scan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // storeid: '',
      // member:  this.props.navigation.state.params

    }
  }

  // constructor(props) {
  //   super(props);
  //   this.state = {
  //       storeid: '',
  //   }
  // }
  // async check() {
  //   await firebase.database().ref("balances").on("value", (data) => {
  //     let bal = data.toJSON()
  //     let pass = bal.balance
  //     if (pass >= this.state.money) {
  //       this.props.navigation.navigate('Bill');
  //     }
  //     else {
  //       alert("check your balance!!")
  //     }

  //   })

  // }
   _handleBarCodeRead = async ({ data }) => {
    // console.log("----->"+data)
    let localstoreid = data
    // alert(data)
    await firebase.database().ref("users").on("value", (data) => {
      // console.log("2----->"+localstoreid)
      let users = data.toJSON()
      // console.log("----------scan----------")
      // console.log(users)
      // console.log(users)
      // alert(data)
      let store = users[localstoreid]
      // console.log(users[localstoreid])
      this.props.navigation.navigate('Payment', {store: store,storeid: localstoreid});
      // console.log("----->"+store)

    })
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ flex: 1 }}>

        <ImageBackground
          source={require('./img/scan.jpg')}
          style={styles.container1} >
          <View style={styles.top}>
              {/* <Text style={styles.header}>S C A N</Text> */}

         </View>
          <View style={styles.container}>
          


            <BarCodeScanner
              onBarCodeRead={this._handleBarCodeRead}
              style={StyleSheet.absoluteFill}
            />


          </View>
          <View style={styles.container2}>



            <TouchableOpacity
              style={styles.btn}
              onPress={() => navigate('User')}>
              <Text style={{ color: "#fff" }}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>

    );
  }



  
}


const styles = StyleSheet.create({
  header: {
    color: '#fff',
    fontSize: 28,
    borderColor: '#fff',
    borderWidth: 2,
    padding: 20,
    paddingLeft: 40,
    paddingRight: 40,
    backgroundColor: 'rgba(255,255,255,0.1)'
  },
  menuItem: {
    width: '33.333333%',
    height: '50%',
    padding: 20,
  },
  image: {
    width: '33.333333%',
    height: '50%',
    opacity: 0.8,
    // borderColor: '#fff',
    // borderWidth: 3,
    marginVertical: 15
  },
  container1: {
    flex: 1,
    width: '100%',
    height: '100%',

  },
  container: {
    flex: 2,
    paddingBottom: 60,
    paddingTop: 40,
    height: '30%',
    // flexDirection: 'row',
    // flexWrap: 'wrap',
    alignItems: 'center',
    // justifyContent: 'center',
    marginVertical: 5,
    marginHorizontal: 40
  },

  container2: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#2896d3',
    paddingLeft: 40,
    paddingRight: 40,
  },

  text: {
    color: '#fff',
  },
  top: {
    height: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    alignSelf: 'stretch',
    padding: 16,
    marginBottom: 20,
    backgroundColor: '#fff',
    marginVertical: 10,
    borderRadius: 25,
  },
  btn: {
    alignSelf: 'stretch',
    backgroundColor: '#173970',
    padding: 20,
    alignItems: 'center',
    borderRadius: 25,
  }
});
