import React from 'react';
import { StyleSheet, Text, View,
        TextInput,
        KeyboardAvoidingView, 
        TouchableOpacity, 
        ImageBackground,Button,Image,
        AsyncStorage } from 'react-native';
import { StackNavigator } from 'react-navigation';
import firebase from 'firebase';


export default class Profile extends React.Component {



  
  async loaddata(){
    let myuserid =  await AsyncStorage.getItem('@UserId:key');
    this.setState({userid: myuserid})
  }
  constructor(props) {
    super(props);
    this.state = {
      userid:''
    }
    this.loaddata()
  }


  
  render() {
    const { params } = this.props.navigation.state;
    const username = params ? params.itemId : null;
      const { navigate } = this.props.navigation;
      return (
  <ImageBackground 
        source={require('./img/bg.jpg')}
        style={styles.container}           >
        <View style={styles.overlayContainer}>
         
         <View style={styles.top}>
              <Text style={styles.header}>M E N U</Text>
              <Image style={styles.logo} source={require('./img/logo.png')} />
         </View>
         
  
        <View style={styles.menuContainer}>
        <View style={styles.menuItem}>
          <ImageBackground
          source={require('./img/user.png')}
          style={styles.image}>
           <Button title='' onPress={() => navigate('User',this.props.navigation.state.params)} style={{alignItems: 'center',justifyContent: 'center',display: 'flex',}}/>
          </ImageBackground>
          </View>
          
          <View style={styles.menuItem}>
          <ImageBackground
          source={require('./img/scan.png')}
          style={styles.image}>
           <Button title='' onPress={() => navigate('Scan')} style={{alignItems: 'center',justifyContent: 'center',display: 'flex',}}/>
          </ImageBackground>
          </View>

          <View style={styles.menuItem}>
          <Button title='History' onPress={() => navigate('History',this.props.navigation.state.params)} style={{alignItems: 'center',justifyContent: 'center',display: 'flex',}}/>
          </View>

          <View style={styles.menuItem}>
          <Button title='payment' onPress={() => navigate('Payment',this.props.navigation.state.params)} style={{alignItems: 'center',justifyContent: 'center',display: 'flex',}}/>
          </View>
          
  
          <View style={styles.menuItem}>
          <ImageBackground
          source={require('./img/logout.png')}
          style={styles.image}
          >
           <Button title='' onPress={() => navigate('Home')} style={styles.image}/>
          </ImageBackground>
          </View>
          
              
        </View>
  
        </View>
        </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
  container: {
      flex: 1,
      width: '100%',
      height: '100%',
    },
overlayContainer: {
  flex: 1,
  // backgroundColor: 'rgba(47,163,218,0.4)'
},
top: {
  marginVertical: 30,
  height: '50%',
  alignItems: 'center',
  justifyContent: 'center',
},
header: {
  color: '#fff',
  fontSize: 28,
  borderColor: '#fff',
  borderWidth: 2,
  padding: 20,
  paddingLeft: 40,
  paddingRight: 40,
  backgroundColor: 'rgba(255,255,255,0.1)'
},
menuContainer: {
  // paddingTop: 20,
  height: '40%',
  flexDirection: 'row',
  flexWrap: 'wrap',
  // alignItems: 'flex-end',
},
menuItem: {
  width: '33.333333%',
  height: '50%',
  padding: 20,
},
image: {
  width: '100%',
  height: '100%',
  opacity: 0.8,
  borderColor: '#fff',
  borderWidth: 3,
},
centerText: {
  flex: 1,
  fontSize: 18,
  padding: 32,
  color: '#777',
},
textBold: {
  fontWeight: '500',
  color: '#000',
},
buttonText: {
  fontSize: 21,
  color: 'rgb(0,122,255)',
},
buttonTouchable: {
  padding: 16,
},
logo: {
  marginVertical: 30,
  alignItems: 'center',
  justifyContent: 'center',
  width: 150,
  
  height: 150
},
});

