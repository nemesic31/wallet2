import React from 'react';
import {
    StyleSheet, Text, View,
    TextInput,
    KeyboardAvoidingView,
    TouchableOpacity,
    AsyncStorage, ImageBackground, Image,
    Button
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { parse } from 'ipaddr.js';
import firebase from 'firebase';

export default class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }



    render() {
        const { navigate } = this.props.navigation;
        return (
            <ImageBackground
                source={require('./img/login.jpg')}
                style={styles.container1}           >
                <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>

                    <View style={styles.container}>
                        {/* <Image style={styles.logo} source={require('./img/logo.png')} /> */}
                        <TextInput
                            style={styles.textInput} placeholder='Username'
                            onChangeText={(username) => this.setState({ username })}

                            underlineColorAndroid='transparent'
                        />

                        <TextInput
                            style={styles.textInput} placeholder='Password'
                            onChangeText={(password) => this.setState({ password })}
                            secureTextEntry={true}
                            underlineColorAndroid='transparent'
                        />

                        <TouchableOpacity
                            style={styles.btn}
                            onPress={() => { this.login() }}>
                            <Text style={{ color: "#fff" }}>Log In</Text>
                        </TouchableOpacity>
                        <View style={styles.signupTextCont}>
                            {/* <Text style={styles.signupText}>Don't have an account yet? </Text>
                <TouchableOpacity onPress={() => navigate('Signup')}>
                    <Text style={styles.signupButton}>Signup </Text>
                </TouchableOpacity> */}
                        </View >
                        {/* <Button title='login' onPress={() => navigate('Profile')} style={{ alignItems: 'center', justifyContent: 'center', display: 'flex', }} /> */}
                    </View>
                </KeyboardAvoidingView>
            </ImageBackground>
        );
    }
    snapshotToArray(snapshot) {
        var returnArr = [];
      
        snapshot.forEach(function(childSnapshot) {
            var item = childSnapshot.val();
            item.key = childSnapshot.key;
      
            returnArr.push(item);
        });
      
        return returnArr;
      };


    async login() {
         
    



        await firebase.database().ref("users").on("value", (data) => {
            let users = data.toJSON()
            // console.log("-----------Login----------")
            // console.log(user)

            // alert(user[this.state.username])
            // console.log(this.state.username)

            let user = users[this.state.username]
            // console.log(user.histories)
            
            // var res = Object.keys(user.histories)
            // // iterate over them and generate the array
            // .map(function(k) {
            //   // generate the array element 
            //   return [user.histories[k]];
            // });
              
                
            // console.log(res)
            if(this.state.username == 0){
                alert("please input username")
            }else
            
            if (user.password == this.state.password) {

                    AsyncStorage.setItem('@User:key', JSON.stringify(user));
                    AsyncStorage.setItem('@UserId:key', this.state.username);
            //    console.log("------------->" + user)
            //    console.log( user)
               
                this.props.navigation.navigate('User', user);
            }
            else {
                
                alert("wrong password!!")
            }



        }


        )
        // console.log("-----------Login----------")
        //      let user = await fetch('https://mwallet-1234.firebaseio.com/users.json')
        //         alert(user.tor);
        // JSON parse
        //   .then((response) => response.json())
        //   .then((res) => {

        //     alert(res.message);

        //     if (res.success === true) {
        //         AsyncStorage.setItem('user',res.user);
        //         this.props.navigation.navigate('Profile');
        //     }

        //     else {
        //         alert(res.message);
        //         }
        //       })


        //     .done();

        //   }
        // }
    }
}

const styles = StyleSheet.create({
    signupTextCont: {
        // flexGrow: 1,
        // alignItems: 'flex-end',
        justifyContent: 'center',
        marginVertical: 16,
        flexDirection: 'row',

    },
    signupText: {
        color: "#3e5c7a",
        fontSize: 16,
    },
    signupButton: {
        color: "#203e5b",
        fontSize: 16,
        fontWeight: '500',
    },
    logo: {
        width: 150,
        height: 150
    },
    container1: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    wrapper: {
        flex: 1,

    },
    container: {
        paddingTop: 190,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40,
    },
    header: {
        fontSize: 24,
        marginBottom: 60,
        color: '#fff',
        fontWeight: 'bold',
    },
    textInput: {
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#fff',
        marginVertical: 10,
        borderRadius: 25,
    },
    btn: {
        alignSelf: 'stretch',
        backgroundColor: '#173970',
        padding: 20,
        alignItems: 'center',
        borderRadius: 25,
    }
});
