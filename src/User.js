import React from 'react';
import { StyleSheet, Text, View,
        TextInput,
        KeyboardAvoidingView, 
        TouchableOpacity, 
        ImageBackground,Button,Image, 
        AsyncStorage } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Card } from 'react-native-elements';
import QRCode from 'react-native-qrcode';


export default class User extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: '',
      id:''

    }
    

   
    this.loaddata()
    // console.log("3-------------->" + this.state.user.firstname)
}

  async loaddata(){
    
    let userid =  await AsyncStorage.getItem('@User:key');
    let myuserid =  await AsyncStorage.getItem('@UserId:key');
    this.setState({user: JSON.parse(userid)})
    this.setState({id: myuserid})
    // this.setState({historydate: userid.history[date]})
    
   
    // console.log("4-------------->" + userid)
   
    
  }




  render() {
    const { navigate } = this.props.navigation;
  
    return (
      <ImageBackground 
        source={require('./img/newbg.jpg')}
        style={styles.container1}           >
         <View style={styles.container}>
         {/* <Image style={styles.logo} source={require('./img/user.png')} /> */}
         {/* <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({text: this.state.id})}
          value={this.state.text}
        /> */}
        <View style={{flex: 1,alignItems: "center",marginVertical: 50}}>
          <QRCode style={{marginVertical: 10,alignItems: "center"}}
          value={this.state.id}
          size={75}
          bgColor='black'
          fgColor='white'/>
          
      <View style={{marginVertical: 50,alignItems: "center"}}>

        <Text style={{color:"#fff",fontSize: 22,alignItems: "center"}}>{this.state.user.balance} ฿</Text>
        <Text style={{color:"#fff"}}>{this.state.user.firstname} {this.state.user.lastname}</Text>
        
      </View>
        

        <View style={{ marginVertical: 70,alignItems: "center"}}>
{/* <Text style={{color:"#13293D",fontSize: 22,alignItems: "center"}}>฿</Text> */}
<Card title='Receipt'>
       <Text style={{color:"#13293D",alignItems: "center"}}>Partner ID: {this.state.user.Iduser}</Text>
 <Text style={{color:"#13293D",alignItems: "center"}}>{this.state.user.amount} bath</Text>
 <Text style={{color:"#13293D",alignItems: "center"}}>date: {this.state.user.date}</Text>
</Card>  

<View style={{ marginVertical: 20}}>
<View style={{flex: 1, flexDirection: 'row'}}>
<View style={{paddingRight: 20}}>
   <TouchableOpacity
        style={styles.btn}
        onPress={() => navigate('Scan')}>
        <Text style={{color:"#fff"}}>Scan</Text>
        </TouchableOpacity>
        </View>
        <View style={{paddingRight: 20}}>
   <TouchableOpacity
        style={styles.btn}
        onPress={() => navigate('History')}>
        <Text style={{color:"#fff"}}>History</Text>
        </TouchableOpacity>
        </View>
  <TouchableOpacity
        style={styles.btn}
        onPress={() => navigate('Home')} >
        <Text style={{color:"#fff"}}>Sign Out</Text>
        </TouchableOpacity>
</View>
        
        
        </View>
</View>


        </View>
        
       
        {/* <Card title='Profile' style={styles.card}>
        <Text>firstname : {this.state.user.firstname}</Text>
        <Text>lastname : {this.state.user.lastname}</Text>
        <Text>balance : {this.state.user.balance} bath</Text>
       </Card> */}
      





        
          </View>
       </ImageBackground>
    );
  }
}



const styles = StyleSheet.create ({
  
  card: {
    width: '80%',
    height: '50%',
  },
  menuItem: {
    width: '33.333333%',
    height: '50%',
    padding: 20,
  },
  image: {
    width: '100%',
    height: '100%',
    opacity: 0.8,
    borderColor: '#fff',
    borderWidth: 3,
  },
  container1: {
    
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    // backgroundColor: '#2896d3',
    paddingLeft: 10,
    paddingRight: 10,
    marginVertical: 40
  },

   text: {
       color: '#fff',
   },
   btn: {
    // flexDirection: 'row',
    // marginVertical: 10,
    // alignSelf: 'stretch',
    width: 100, 
    height: 60,
    backgroundColor: '#173970',
    padding: 20,
    alignItems: 'center',
    borderRadius: 50,
},
logo: {
  width: 150,
  height: 150
},
});
