import React from 'react';
import {
  StyleSheet, Text, View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  ImageBackground, Button, Image,
  AsyncStorage
} from 'react-native';
import { Card } from 'react-native-elements';
import { StackNavigator } from 'react-navigation';
import Scan from './Scan';
import firebase from 'firebase';


export default class Payment extends React.Component {
 
  async loaddata(){
    let myuser =  await AsyncStorage.getItem('@User:key');
    let myuserid =  await AsyncStorage.getItem('@UserId:key');
    this.setState({user: JSON.parse(myuser)})
    this.setState({userid: myuserid})
    
    // console.log("4-------------->" + myuserid)
    // console.log( this.state.userid)

  }
  constructor(props) {
    super(props);
    // console.log(this.props.navigation.state)
    // let userid = await AsyncStorage.getItem('@UserId:key');
    // console.log("-------payment------->")
    // console.log("4-------------->" + userid)

    this.state = {
      storeid: this.props.navigation.state.params.storeid,
      store: this.props.navigation.state.params.store,
      user: '',
      price: '',
      userid:''
      // pincodes: ''



    }
    this.loaddata()
    // console.log("5-------------->" + userid)
  }




  // showName = ({data}) => {
  //         show(' ${data} ')
  // }

  async updateBal(amount) {
    // console.log( "---------------->" + this.state.userid)
    await firebase.database().ref("users/" + this.state.userid).update({balance: amount});

  }

 


  async updateBalStore(amountStore) {

    // console.log("2---------------->" + this.state.storeid)
    await firebase.database().ref("users/" + this.state.storeid).update({balance: amountStore});

  }



  async check() {
      let amount = this.state.user.balance
      let amountStore = this.state.store.balance
      // let pin = this.state.user.pincode
      // console.log("------calculate-------->")

      if(this.state.price <= 0){
        alert("please input your balance")
    }else
      if (amount >= this.state.price) {

        amount = amount - this.state.price
        amountStore = parseFloat(amountStore) + parseFloat(this.state.price)
        // console.log("6-------------->" + amount)
        this.updateBal(amount)
        this.updateBalStore(amountStore)
        this.history()
        this.historystore()
        this.historyamounttest()
        this.historydatetest()
        this.historystoreamounttest()
        this.historystoredatetest()
        this.historyIdusertest()
        this.historyIdstoretest()
        this.historiesamountstore()
        this.historiesamountuser()
       

        this.props.navigation.navigate('Bill');
      }
      else {
        alert(" check your balance!!")
        // console.log("7-------------->" + amount)
      }

    

  }

  async historiesamountstore() {
    let d = new Date()
    let currentdate =d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()
    let myhistorystore ={amount:"+" + this.state.price,Iduser: this.state.userid,date: currentdate} 
    await firebase.database().ref("users/" + this.state.storeid+ "/histories").push(myhistorystore);
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }

  async historiesamountuser() {

    let d = new Date()
    let currentdate =d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()
    let myhistorystore ={amount:"-" + this.state.price,Iduser: this.state.storeid,date: currentdate} 
    await firebase.database().ref("users/" + this.state.userid+ "/histories").push(myhistorystore);
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }
 

  async historystore() {


    let myhistorystore ={amount:"+" + this.state.price,date: new Date()} 
    await firebase.database().ref("users/" + this.state.storeid).update({history: myhistorystore});
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }
//update user
  async historyamounttest() {

    await firebase.database().ref("users/" + this.state.userid).update({amount: "Withdraws: " + this.state.price});
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }
  async historyIdusertest() {

    await firebase.database().ref("users/" + this.state.userid).update({Iduser: this.state.storeid});
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }
  async historydatetest() {

    await firebase.database().ref("users/" + this.state.userid).update({date: new Date()});
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }


//update store
  async historystoreamounttest() {

    await firebase.database().ref("users/" + this.state.storeid).update({amount: "Deposit: " + this.state.price});
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }
  async historystoredatetest() {

    await firebase.database().ref("users/" + this.state.storeid).update({date: new Date()});
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }
  async historyIdstoretest() {

    await firebase.database().ref("users/" + this.state.storeid).update({Iduser: this.state.userid});
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }



  async history() {


    let myhistory ={amount:"-" + this.state.price,date: new Date()} 
    await firebase.database().ref("users/" + this.state.userid).update({history: myhistory});
    // AsyncStorage.setItem('@User:key', JSON.stringify(user));
  }

  render() {
    const { navigate } = this.props.navigation;
    // console.log(this.state.store.firstname)
    return (
      <ImageBackground
        source={require('./img/payment.jpg')}
        style={styles.container}           >
        <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>
        <View style={styles.overlayContainer}>
        
        <View style={styles.top}>
        
<Card title="Payment" >
          
            <View>
              <Text style={styles.header}>To: {this.state.store.firstname}</Text>
              <Text style={styles.header}>ID: {this.state.storeid}</Text>
              <Text style={styles.header}>Your amount: {this.state.user.balance}</Text>
            
            </View>
            <TextInput
              style={styles.textInput} placeholder='Bath'
              keyboardType = 'numeric'
              onChangeText={(price) => this.setState({ price })}
              
              underlineColorAndroid='transparent'
            />

          </Card>

          
          </View>

<View style={{marginVertical: 40,alignItems: 'center' }}>
<View style={{flex: 1, flexDirection: 'row'}}>
<View style={{paddingRight: 20}}>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => { this.check() }}>
            <Text style={{ color: "#fff" }}>Pay</Text>
          </TouchableOpacity>
 </View>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => navigate('User')}>
              <Text style={{ color: "#fff" }}>Cancel</Text>
            </TouchableOpacity>
          </View>
          </View>
          




        </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  overlayContainer: {
    // alignItems: 'center',
    marginVertical: 20,
   justifyContent: 'center'
    // flex: 1,
    // backgroundColor: 'rgba(47,163,218,0.4)'
  },
  top: {
    // height: '50%',
    // alignItems: 'center',
    // justifyContent: 'center',
    // marginVertical: 20
  },
  header: {
    // color: '#fff',
    fontSize: 18,
    // borderColor: '#fff',
    // borderWidth: 2,
    // padding: 20,
    paddingLeft: 40,
    paddingRight: 40,
    // paddingVertical: 40,
    backgroundColor: 'rgba(255,255,255,0.1)'
  },
  menuContainer: {
    height: '40%',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  menuItem: {
    width: '33.333333%',
    height: '50%',
    padding: 20,
  },
  image: {
    width: '100%',
    height: '100%',
    opacity: 0.8,
    borderColor: '#fff',
    borderWidth: 3,
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 15,
    color: '#fff',
  },
  buttonTouchable: {
    padding: 16,
  },
  textInput: {
    alignSelf: 'stretch',
    padding: 16,
    marginBottom: 20,
    backgroundColor: '#247BA0',
    marginVertical: 20,
    borderRadius: 25,
  },
  btn: {
    width: 100, 
    height: 60,
    alignSelf: 'stretch',
    backgroundColor: '#173970',
    padding: 20,
    alignItems: 'center',
    borderRadius: 25,
  }
});

